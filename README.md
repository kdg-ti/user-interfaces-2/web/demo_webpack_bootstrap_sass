# About

## Usage

-   From the project directory enter the following commands:

```
$ npm i
$ npm run dev
o #to open in the browser
```

## Technologies used

- vite
- ts
- npm
- bootstrap
- bootstrap-icons
- sass
