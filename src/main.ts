import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "bootstrap";
import "./css/style.scss";
import { setupCounter } from "./ts/counter.ts";

setupCounter();
