function counter(element: HTMLButtonElement) {
  let counter = 0
  const setCounter = (count: number) => {
    counter = count
    element.innerHTML = `count is ${counter}`
  }
  element.addEventListener('click', () =>
    setCounter(counter + 1))
  setCounter(0)
}

export function setupCounter() {
  counter(document.querySelector<HTMLButtonElement>('#counter')!);
}